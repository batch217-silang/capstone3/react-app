import { Card, Button, Col, Row } from 'react-bootstrap';
import { Link } from "react-router-dom";
export default function BookCard({bookProp}) {


	const { _id, title, author, genre, price, slots } = bookProp;


    return (
    <Row xs={1} md={2} lg={4} className="g-4">
      {Array.from({ length: 4 }).map((_, idx) => (
        <Col>
        <Card bordered className="shadow-lg shadow-sm mb-5" style={{ width: '18rem' }} >
            <Card.Body>
                <Card.Title>
                    {title}
                </Card.Title>
                <Card.Subtitle>
                    Author:
                </Card.Subtitle>
                <Card.Text>
                    {author}
                </Card.Text>
                <Card.Subtitle>
                    Genre:
                </Card.Subtitle>
                <Card.Text>
                    {genre}
                </Card.Text>
                <Card.Subtitle>
                    Price:
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>
                <Button as={Link} to={`/books/${_id}`} variant="success">Add to Cart</Button>
            </Card.Body>
        </Card>
    </Col>
      ))}
    </Row>
    );
}