import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home(){
	const data = {
		title: "DigiBooks",
		content: '"There is no friend as loyal as a book" ',
		author: "by Ernest Hemingway",
		destination: "/login",
		label: "Grab yours Now!"
	}

	return(
		<>	
			<Banner bannerProp={data}/>
			<Highlights />
		</>
	)
}